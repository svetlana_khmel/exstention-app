Thes repo is for demo-projects:

* 1) Crome extention gets current url, and referer and sends them to the node server.
Then waits for the category name and shows the icon according to the category.

* 2) Node server gets links from the chrome extention, sends link to the python server, 
wait from the python server a response with category and sends the category to the 
chrome extention.

* 3) Python server gets link from node server and sends back to the node server.

---------------------------------
1) To create an image, create Dockerfile (configurale file)

- To build an image (configuration - Dockerfile):
docker build -t python-server .

- To see images
docker images

- Run a container from that image
 docker run -p 5000:5000 python-server

- Build the container (Instance of image, file - docker-compose.yml)
docker-compose build

- Start all the containers:
docker-compose up

-Stop all the containers
Docker-compose stop

- See running images
docker ps
docker container ls

—> Better to start separately
docker-compose up -d mongo
docker-compose up -d app
docker-compose up -d client


-Delete image
docker rmi c32705f7fde6

Docker ——> documentation

docker-compose up —>starts all the containers
docker-compose up -d mongo —>we are gonna run mongo first

docker ps ——> Checks if the container started

docker-compose up -d app

::::::::::::::::
****** Докеризация веб-приложения Node.js *****
https://nodejs.org/ru/docs/guides/nodejs-docker-webapp/


******************

`
The web service
The first definition in the file is for the web service. In order, the directives mean:

build: build it from the Dockerfile in the parent directory
links: link to the database container
ports: map the external port 8000 to the internal port 80
volumes:
map the parent directory on the host to /app in the container, with read and write access
map the data directory on the host to /data in the container, with read and write access
command: by default, when the command docker-compose run is issued, execute python manage.py runserver 0.0.0.0:80
env_file: use the .env-local to supply environment variables to the container

------------------------------
*** Install docker machine:***
* You need to install Docker Machine first on your local machine. If you use Ubuntu, just use this snippet (Update the version from the Official Repository Releases if needed) :

$ curl -L https://github.com/docker/machine/releases/download/v0.16.0/docker-machine-`uname -s`-`uname -m` >/tmp/docker-machine &&
chmod +x /tmp/docker-machine &&
sudo cp /tmp/docker-machine /usr/local/bin/docker-machine

`
:::::::::::::::::::::::::::::::::::
`
* See all containers:
docker images

* See all running containers:
docker ps / 

* See all containers:
docker ps -a

* Build image
docker build --tag python-server .
 docker build --tag python-server .

* Run image
docker run --net host -p 8300:8300 d79870b3795b

* Up containers
docker-compose up -d

* Down container
docker-compose down -v

* Run with network
docker run -p 8300:8300 --network=backend-net --name=python-server 6c2a15180997

*TO UNTUG AND DELETE IMAGES
docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
docker rmi c565603bc87f

docker rmi $(docker images -q) -f


* One liner to stop / remove all of Docker containers:
	
docker rm $(docker ps -a -q)

* Clean cache
docker system prune -a

docker exec -it <container-name> bash
docker stop <container-name>
docker rm <container-name>

lsof -n -i4TCP:8080
PID is second field. Then, kill that process:
kill -9 PID



`






* Need to create network: 
* docker network create backend_default

docker compose up -d
